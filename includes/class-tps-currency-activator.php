<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.theparentsshop.com/
 * @since      1.0.0
 *
 * @package    Tps_Currency
 * @subpackage Tps_Currency/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Tps_Currency
 * @subpackage Tps_Currency/includes
 * @author     Haris Kaklamanos <ckaklamanos@theparentsshop.com>
 */
class Tps_Currency_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {
        Tps_Currency_Rates::register_corn_job();
        Tps_Currency_Rates::get_rates_from_service();

        //Add _tps_currency post meta to external products without it
        Tps_Currency_Rates::add_currency_post_meta();
	}

}
