<?php

/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://www.theparentsshop.com/
 * @since      1.0.0
 *
 * @package    Tps_Currency
 * @subpackage Tps_Currency/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    Tps_Currency
 * @subpackage Tps_Currency/includes
 * @author     Haris Kaklamanos <ckaklamanos@theparentsshop.com>
 */
class Tps_Currency {

	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      Tps_Currency_Loader    $loader    Maintains and registers all hooks for the plugin.
	 */
	protected $loader;

	/**
	 * The unique identifier of this plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $plugin_name    The string used to uniquely identify this plugin.
	 */
	protected $plugin_name;

	/**
	 * The current version of the plugin.
	 *
	 * @since    1.0.0
	 * @access   protected
	 * @var      string    $version    The current version of the plugin.
	 */
	protected $version;

	/**
	 * Define the core functionality of the plugin.
	 *
	 * Set the plugin name and the plugin version that can be used throughout the plugin.
	 * Load the dependencies, define the locale, and set the hooks for the admin area and
	 * the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function __construct() {

		$this->plugin_name = 'tps-currency';
		$this->version = '2.0.0';

		$this->load_config_file();

		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
		$this->define_public_hooks();

	}

	/**
	 * Load the config.php file , if exists
	 * @access   private
	 */
	private function load_config_file() 
	{
		$config_file_path = plugin_dir_path( dirname( __FILE__ ) ) . 'config.php';

		if ( file_exists( $config_file_path ) ) 
		{
			require_once $config_file_path;
		}

	}
	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - Tps_Currency_Loader. Orchestrates the hooks of the plugin.
	 * - Tps_Currency_i18n. Defines internationalization functionality.
	 * - Tps_Currency_Admin. Defines all hooks for the admin area.
	 * - Tps_Currency_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {

		/**
		* The class responsible for adding product currency meta box
		*/
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/post-meta/class-tps-post-meta-product-currency.php';

        /**
         * The class responsible for currency rates
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/currency-rates/class-tps-currency-rates.php';

		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-currency-loader.php';

		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-tps-currency-i18n.php';

		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-tps-currency-admin.php';

		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-tps-currency-public.php';

        /**
         * The class responsible for showing the selected currency symbol
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/classes/class-tps-currency-conversion.php';
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/classes/class-tps-currency-modal.php';

		$this->loader = new Tps_Currency_Loader();

	}

	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the Tps_Currency_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {

		$plugin_i18n = new Tps_Currency_i18n();

		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );

	}

	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {

		$plugin_admin = new Tps_Currency_Admin( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );
		$this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );
		
		//Show Currency Post meta via the available WC action
		$this->loader->add_action( 'woocommerce_product_options_pricing', 'Tps_Post_Meta_Product_Currency', 'render' );
		//Save Currency Post Meta
		$this->loader->add_action( 'save_post', 'Tps_Post_Meta_Product_Currency' , 'save' ,  10, 2 ) ;

		$this->loader->add_action( 'admin_init', 'Tps_Currency_Rates' , 'register_rate_options' );

		$this->loader->add_action( 'tps_currency_get_rates', 'Tps_Currency_Rates' , 'get_rates_from_service' );

	}

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {

		$plugin_public = new Tps_Currency_Public( $this->get_plugin_name(), $this->get_version() );

		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );

        // filter in woocommerce/single-product/price.php and woocommerce/loop/price.php of theme
		$this->loader->add_filter( 'tps_get_price_html', 'Tps_Currency_Conversion', 'show_converted_price_html', 10, 1 );

		// action in woocommerce/single-product/price.php of theme
		$this->loader->add_action( 'tps_price_approximately', 'Tps_Currency_Conversion', 'show_converted_currencies', 10, 1 );

		// filter in woocommerce/single-product/price.php of theme
        $this->loader->add_filter( 'tps_price_meta', 'Tps_Currency_Conversion', 'set_price_meta', 10, 1 );

        
        $tps_currency_modal = new Tps_Currency_Modal( $this->get_plugin_name(), $this->get_version() );

        $this->loader->add_action( 'wp_footer', $tps_currency_modal, 'add_modal_to_footer' );

        $this->loader->add_action( 'wp_enqueue_scripts', $tps_currency_modal, 'enqueue_modal_script' );
        $this->loader->add_action( 'wp_ajax_tps_save_currency_cookie', $tps_currency_modal, 'set_currency_cookie');
        $this->loader->add_action( 'wp_ajax_nopriv_tps_save_currency_cookie', $tps_currency_modal, 'set_currency_cookie');

	}

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}

	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}

	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    Tps_Currency_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

}
