<?php

/**
 * Fired during plugin deactivation
 *
 * @link       http://www.theparentsshop.com/
 * @since      1.0.0
 *
 * @package    Tps_Currency
 * @subpackage Tps_Currency/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Tps_Currency
 * @subpackage Tps_Currency/includes
 * @author     Haris Kaklamanos <ckaklamanos@theparentsshop.com>
 */
class Tps_Currency_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {
        Tps_Currency_Rates::unregister_corn_job();
	}

}
