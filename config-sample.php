<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$tps_currencies_array = array (
	'EUR',
	'USD'
);

define( 'TPS_CURRENCIES', serialize ( $tps_currencies_array ) );
define( 'TPS_SECONDARY_CURRENCY', 'USD' );

define( 'TPS_OPEN_EXCHANGE_RATES_URL', 'https://openexchangerates.org/api/latest.json' );
define( 'TPS_OPEN_EXCHANGE_RATES_APP_ID', '' );