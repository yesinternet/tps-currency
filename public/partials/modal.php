<div class="modal fade" id="tps_currency_modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <span class="modal-title" id="myModalLabel"><?php _e('Choose your currency'); ?></span>
            </div>
            <div class="modal-body">
                <?php $currencies = Tps_Currency_Modal::get_currencies_for_conversion(); ?>
                <ul class="list-unstyled tps-currency-list">
                <?php foreach ($currencies as $currency_code => $currency): ?>
                    <li>
                        <a href="#" data-curcode="<?php echo $currency_code; ?>" class="tps-currency-select-link">
                            <?php $currency_symbol = get_woocommerce_currency_symbol($currency_code); ?>
                            <?php echo '<span>' . __($currency) . '</span> <small>' . __($currency_code) . ' (' . $currency_symbol . ')</small>' ; ?>
                        </a>
                    </li>
                <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
</div>

