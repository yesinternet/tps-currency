<?php

class Tps_Currency_Modal {

    private $plugin_name;
    private $version;

    public function __construct( $plugin_name, $version ) {

        $this->plugin_name = $plugin_name;
        $this->version = $version;

    }

    public function add_modal_to_footer() {

        if(!(is_single() && get_post_type() == 'product')){
            return;
        }

        $dir_path = plugin_dir_path( dirname(__FILE__) );

        require_once($dir_path . "/partials/modal.php");
    }

    public static function get_currencies_for_conversion() {
        $currencies_to_show = array();

        $currencies = defined('TPS_CURRENCIES') ? unserialize(TPS_CURRENCIES) : array();

        $currencies_names = get_woocommerce_currencies();

        foreach ($currencies as $currency){
            $currencies_to_show[$currency] = $currencies_names[$currency];
        }

        return $currencies_to_show;
    }

    public function enqueue_modal_script() {
        $ajax_nonce = wp_create_nonce( "Tps_Secure_Currency" );
        $shop_currency = get_woocommerce_currency();

        $tps_currency_ajax_object = array(
            'ajax_url' => admin_url( 'admin-ajax.php' ),
            'secure' =>  $ajax_nonce,
            'shop_currency' => $shop_currency,
            'secondary_currency' => defined('TPS_SECONDARY_CURRENCY') ? TPS_SECONDARY_CURRENCY : $shop_currency
        );

        wp_enqueue_script( 'tps-currency-modal', plugin_dir_url( dirname(__FILE__) ) . 'js/tps-currency-modal.js', array( 'jquery', 'jquery-cookie' ), $this->version, true );
        wp_localize_script( 'tps-currency-modal', 'tps_currency_ajax_object', $tps_currency_ajax_object );
    }

    public function set_currency_cookie() {
        check_ajax_referer('Tps_Secure_Currency', 'secure');
        $currency_code = $_POST['currency_code'];
        setcookie('tps_currency_code', $currency_code, time() + (30 * 24 * 60 * 60), '/' );
        echo $currency_code;
        wp_die();
    }
}