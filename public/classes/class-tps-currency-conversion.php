<?php

class Tps_Currency_Conversion {

    public static function show_converted_price_html($price_html) {

        global $product;

        //$tps_product = clone $product;
        $product->tps_original_price = $product->get_price();
        $tps_product = self::convert_product_price($product);

        if(empty($tps_product)) return $price_html;

        $price_html = $tps_product->get_price_html();

        return $price_html;
    }

    public static function show_converted_currencies($product) {

        $original_price = !empty($product->tps_original_price) ? $product->tps_original_price : $product->get_price();
        $price = wc_get_price_to_display($product, array('price' => $original_price));

        $prices = self::convert_price($price);
        $shop_currency = get_woocommerce_currency();

        echo '<small class="tps-converted-currency tps-converted-' . $shop_currency . '">';
        echo '    <a href="#" data-toggle="modal" data-target="#tps_currency_modal">' . $shop_currency . '</a>';
        echo '</small>';

        if(!empty($prices)) {
            echo '<span class="tps-converted-price" style="display:none;">' . __('Approximately ', 'tps-currency');
            foreach($prices as $cur => $pr){
                $class = "tps-converted-currency tps-converted-" . $cur;
                echo '<span class="' . $class . '">' . $pr . ' <a href="#" data-toggle="modal" data-target="#tps_currency_modal">' . $cur .'</a></span>';
            }
            echo '</span>';
        }
    }

    public static function convert_product_price(WC_Product $product) {
        $shop_currency = get_woocommerce_currency();
        $product_currency = self::get_selected_currency($product);

        if(empty($product_currency)) return false;

        $rates = get_option('tps_rate_' . $product_currency);
        if(empty($rates)) return $product;

        $rate = $rates[$shop_currency];

        $product->set_price( $product->get_price() * $rate );
        $product->set_regular_price( $product->get_regular_price() * $rate );

        if($product->get_sale_price() > 0) {
            $product->set_sale_price($product->get_sale_price() * $rate);
        }

        return $product;

    }

    public static function convert_price($price) {
        global $product;

        $prices = array();

        $shop_currency = get_woocommerce_currency();
        $product_currency = self::get_selected_currency($product);

        if(empty($product_currency)) return false;

        $rates = get_option('tps_rate_' . $product_currency);

        foreach($rates as $currency => $rate) {
            if($currency == $shop_currency) continue;
            $new_price = $price * $rate;
            $prices[$currency] = wc_price($new_price, array('currency' => $currency));// . ' ' . $currency;
        }

        return $prices;
    }

    public static function set_price_meta($display_price) {
        global $product;

        $tps_product = clone $product;
        $tps_product = self::convert_product_price($tps_product);

        if(empty($tps_product)) return $display_price;

        $display_price = self::tps_wc_price(wc_get_price_to_display($tps_product));

        return $display_price;
    }

    private static function get_selected_currency($product) {

        if(!$product->is_type('external')) return false;

        $post_id = $product->get_id();

        $product_currency = get_post_meta($post_id, '_tps_currency', true);
        $product_currency = !empty($product_currency) ? $product_currency : get_woocommerce_currency();

        return $product_currency;
    }

    private static function tps_wc_price($price, $args = array()) {
        extract( apply_filters( 'wc_price_args', wp_parse_args( $args, array(
            'ex_tax_label'       => false,
            'currency'           => '',
            'decimal_separator'  => wc_get_price_decimal_separator(),
            'thousand_separator' => wc_get_price_thousand_separator(),
            'decimals'           => wc_get_price_decimals(),
            'price_format'       => get_woocommerce_price_format()
        ) ) ) );

        $negative        = $price < 0;
        $price           = apply_filters( 'raw_woocommerce_price', floatval( $negative ? $price * -1 : $price ) );
        $price           = apply_filters( 'formatted_woocommerce_price', number_format( $price, $decimals, $decimal_separator, $thousand_separator ), $price, $decimals, $decimal_separator, $thousand_separator );

        return $price;
    }

}