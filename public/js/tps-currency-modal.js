jQuery(document).ready(function($) {

    var cookie_currency = $.cookie('tps_currency_code');

    tps_change_selected_currency(cookie_currency);

    $("#tps_currency_modal .tps-currency-list [data-curcode]").click(function(event){
        event.preventDefault();
        var currency_code = $(this).data('curcode');
        var shop_currency = tps_currency_ajax_object.shop_currency;

        var data = {
            'action': 'tps_save_currency_cookie',
            'currency_code': currency_code,
            'secure': tps_currency_ajax_object.secure
        };

        $.post(tps_currency_ajax_object.ajax_url, data, function(response) {
            //console.log('Got this from the server: ' + response);
        });

        tps_change_selected_currency(currency_code);

    });

    var nb = $('nav.navbar-fixed-top');
    $('.modal')
        .on('show.bs.modal', function () {
            nb.width(nb.width());
        })
        .on('hidden.bs.modal', function () {
            nb.width(nb.width('auto'));
        });


    function tps_change_selected_currency(currency_code){
        var shop_currency = tps_currency_ajax_object.shop_currency;

        if(currency_code === undefined){
            currency_code = tps_currency_ajax_object.secondary_currency;
        }

        var selected = '.tps-converted-' + currency_code;

        if(currency_code == shop_currency){
            $('.tps-converted-price').hide();
        }else{
            $('.tps-converted-price').show();
        }

        $('#tps_currency_modal').modal('hide');

        $('#tps_currency_modal .tps-currency-select-link').removeClass('tps-curr-selected');
        $('#tps_currency_modal .tps-currency-select-link[data-curcode="' + currency_code +'"]').addClass('tps-curr-selected');

        $('.tps-converted-currency').removeClass('tps-selected');
        $(selected).addClass('tps-selected');
    }

});