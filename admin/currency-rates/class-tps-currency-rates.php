<?php

class Tps_Currency_Rates{

    public static function register_rate_options() {
        $currencies = defined('TPS_CURRENCIES') ? unserialize(TPS_CURRENCIES) : array();

        if(!get_option('tps_currency_rates')) {
            add_option('tps_currency_rates', array());
        }

        foreach ($currencies as $currency){
            if(!get_option('tps_rate_' . $currency)) {
                add_option('tps_rate_' . $currency, array());
                self::convert_rates(get_option('tps_currency_rates'), $currencies);
            }
        }
    }

    public static function register_corn_job() {

        if( !wp_next_scheduled( 'tps_currency_get_rates' ) ) {
            wp_schedule_event( time(), 'daily', 'tps_currency_get_rates' );
        }

    }

    public static function unregister_corn_job() {

        $timestamp = wp_next_scheduled( 'tps_currency_get_rates' );
        wp_unschedule_event( $timestamp, 'tps_currency_get_rates' );

    }

    public static function get_rates_from_service() {
        $url = defined('TPS_OPEN_EXCHANGE_RATES_URL') ? TPS_OPEN_EXCHANGE_RATES_URL : '';
        $app_id = defined('TPS_OPEN_EXCHANGE_RATES_APP_ID') ? TPS_OPEN_EXCHANGE_RATES_APP_ID : '';

        if( empty($url) || empty($app_id) ) return;

        $request = $url . '?app_id=' . $app_id;
        $http_response = wp_remote_get($request);

        if(is_wp_error($http_response) || empty($http_response['body'])) return;

        try {

            $response = json_decode($http_response['body'], true);

            if (!empty($response['error'])) return;

            $currencies = defined('TPS_CURRENCIES') ? unserialize(TPS_CURRENCIES) : array();

            update_option('tps_currency_rates', $response['rates']);

            self::convert_rates($response['rates'], $currencies);

        }catch (Exception $ex){
            return;
        }
    }

    public static function add_currency_post_meta() {

        $args = array(
            'post_type' => 'product',
            'product_type' => 'external'
        );
        $products = get_posts( $args );

        if(empty($products)) return;

        foreach ($products as $product){
            $post_id = $product->ID;
            $post_meta = get_post_meta($post_id, '_tps_currency', true);
            if(empty($post_meta)){
                update_post_meta( $post_id, '_tps_currency', get_option( 'woocommerce_currency' ) );
            }
        }

    }

    private static function convert_rates($rates, $currencies){
        if(empty($rates)) return;

        foreach ($currencies as $currency) {
            $new_rates = array();

            $base = $rates[$currency];

            foreach ($currencies as $cur) {
                $new_rates[$cur] = $rates[$cur] / $base;
            }
            update_option('tps_rate_' . $currency, $new_rates);
        }
    }

}