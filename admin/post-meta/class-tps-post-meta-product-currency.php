<?php
/**
 * Product Currency Post Meta
 *
 * Displays the currency post meta for products.
 *
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

/**
 * Tps_Post_Meta_Product_Currency Class.
 */
class Tps_Post_Meta_Product_Currency {

   /**
	* The HTML for the currency post meta
	*
	*/
	static function render() {

		global $post;

		$product_currency = get_post_meta( $post->ID, '_tps_currency', true );

		if ( empty ( $product_currency ) )
		{
			$product_currency = get_option( 'woocommerce_currency' );
		}

		wp_nonce_field( basename( __FILE__ ), '_tps_currency_nonce' );

		$tps_currencies = ( defined ('TPS_CURRENCIES') ) ? unserialize ( TPS_CURRENCIES ) : array( get_option( 'woocommerce_currency' )) ;

		$currency_code_options = get_woocommerce_currencies();

	?>

        <p class="form-field _tps_currency show_if_external">

            <label for="_tps_currency"><?php _e( 'Currency', 'tps-currency' );?></label>

            <select name="_tps_currency" id="_tps_currency">

                <?php foreach ( $tps_currencies as $key => $code) :?>
                    <option value="<?php echo $code;?>" <?php if ( $product_currency == $code ) echo 'selected="selected"' ;?> ><?php echo $currency_code_options[$code];?> - <?php echo $code;?></option>
                <?php endforeach ;?>

            </select>

        </p>

	<?php 

	}

   /**
	* Save _tps_currency post meta
	*
	*/
	static function save( $post_id ) {

		global $post;
		
		// Verify nonce
		if ( !isset( $_POST['_tps_currency_nonce'] ) || !wp_verify_nonce( $_POST['_tps_currency_nonce'], basename(__FILE__) ) ) {
			return $post_id;
		}
		
		// Check Autosave
		if ( (defined('DOING_AUTOSAVE') && DOING_AUTOSAVE) || ( defined('DOING_AJAX') && DOING_AJAX) || isset($_REQUEST['bulk_edit']) ) {
			return $post_id;
		}

		// Don't save if only a revision
		if ( isset( $post->post_type ) && $post->post_type == 'revision' ) {
			return $post_id;
		}

		// Check permissions
		if ( !current_user_can( 'edit_product', $post->ID ) ) {
			return $post_id;
		}

		$product_currency = sanitize_text_field( $_POST['_tps_currency'] ) ;

		if ( empty ( $product_currency ) )
		{
			update_post_meta( $post->ID, '_tps_currency', get_option( 'woocommerce_currency' ) );
		}
		else
		{
			update_post_meta( $post->ID, '_tps_currency', $product_currency );
		}
	}




}