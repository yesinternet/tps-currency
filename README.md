# How to setup

Create a file named config.php in the root directory of the plugin, according to the provided config-sample.php.

Populate the array variable $tps_currencies_array with the desired currencies. Then set the secondary currency with one of the selected currencies.

Register to Open exchange rates site ([https://openexchangerates.org/](https://openexchangerates.org/)). Get an APP ID and set TPS_OPEN_EXCHANGE_RATES_APP_ID constant.

# How to use

In Product Edit page, in General Tab, select the Currency of the product. Default value is the default currency used by WooCommerce, thus EUR.

# For developers

* The default product currency is equal to the default currency of WooCommerce (in our case EUR).
* All the code applies only to products that are of type 'external'. No code is executed for other types of products, for example Simple products that are used for TPS packages.
* When the plugin is activated these functions are executed:
    1. register_corn_job: Registers a daily cron job that updates the currency rated from the OpenExchangeRates service.
    2. get_rates_from_service: Updates the currency rated from the OpenExchangeRates service.
    3. add_currency_post_meta: Sets _tps_currency post meta to the products that don't have it with the default WooCommerce currency.